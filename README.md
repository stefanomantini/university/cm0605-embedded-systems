# CM0605 Assignment #

Download and unzip SensorSimulation.zip .

This bundle includes a java class SensorSim which simulates a sensor like those we imagine occur in scenarios like the one above. It is a noisy sensor: the value it returns is a emphnominal value plus or minus a a random amount of noise. The noise is simulated using probability-theoretic techniques noise values are random numbers with a normal probability distribution with a con gurable standard deviation. The larger this is, the noisier the simulated sensor.

To use the class, all you need to know about is the two con gurable attributes, the nominal value and the amount of noise (the mean and standard deviation of the normal population being simulated). These are passed into the constructor of SensorSim which then starts the simulation running in its own thread. 4 The nominal value can be adjusted by means of method setNominal(double) which the simulation is running, and the sensor reading (nominal val + noise) retrieved by method double getRdg().

The source code is included for your interest, but you should not change it use the class as a black box. Because it uses a normal probability distribution to do its simulation, the le cumProbs.txt should also be in the working directory.

The class SensorDataDisplay runs a simulated sensor with nominal value ini- tially 100 and noise (standard deviation) 5, and at 500 millisecond intervals retrieves readings and displays them on a graph. Try it now:
java SensorDataDisplay.

Notice that it also outputs data to the console in the format reading (nominal): di erence. By redirecting output to a text le you can log this output for experimental purposes.

In addition, SensorDataDisplay random-walks the nominal sensor value up and down.

A faulty sensor is one which sporadically gives an erroneous reading. FaultySensorSim is a subclass of SensorSim which does this. Two extra at- tributes need to supplied to the constructor (besides the initial nominal value and the noise value) an integer specifying the fault frequency, and a double which is an erroneous value returned by getRdg() instead of the correct (though noisy) one. Fault occurrence is simulated by a Poisson process (like cars passing or rain drops landing) and the parameter is the mean interval between faults, in milliseconds.

Try using a FaultySensorSim instead of a SensorSim instance in SensorDataDisplay. Open the source le SensorDataDisplay.java and nd the line in the runSimulation method which constructs a SensorSim.

sensor = new SensorSim(sensorNom, sensorErr);
Change this to -
sensor = new FaultySensorSim(sensorNom, sensorErr, 10000, 200);

Re-compile and run this to see the e
ect of a faulty sensor with a fault interval of 10000 milliseconds and a fault value of 200. Try other fault frequencies and fault values.

Introducing the Investigation Imagine a set-up in which a sensor monitors some condition of some plant - steam pressure or temperature, say, or current level in the windings of a generator or motor - and the plant must be shut down immediately if the reading becomes dangerously high. 5 The sensor may be \noisy" like the simulated one, and the \noise" level may well be around 2.5 to 5% of the full-scale reading of the sensor. In the simulation, imagine the full-scale reading is 200: so a noisy sensor with noise = 5 is realistic.

Suppose that we have to do an emergency shut-down if the value reaches 150. A noisy sensor might exceptionally trigger a shut-down when the nominal value is only 140 or even less, because \noise" boosts the reading to 150. We may decide we have to live with that. Alternatively we can create several sensors and take as our reading their average. Theory predicts that the \noise" would cancel out, and the noise in the average of n sensor readings is only (1= p n)th of the noise in a single reading. Of course, is is probably uneconomic or impractical to have a very large number of sensors.

There is another potential problem, however. Our sensor might be faulty, and give us a high reading from time to time and this would trigger a shut-down. The shut-downs are required for safety reasons, but are expensive and we really need to avoid false-alarms. We might again employ several sensors, of which one might be faulty. Averaging the readings again will give some protection from a faulty high reading but the average could still be much higher than it should be. A better approach might be to implement a voter which checks all the readings are within tolerance of each other and rejects \out-liers", returning the average of the remainder.

### Warm-up task ###
Record the output of a couple of minutes' run of the basic SensorDataDisplay application as provided (ie, using a SensorSim with nomi- nal value initially = 100 and noise = 5). Redirect the output to a text le thus: java SensorDataDisplay > log.txt. Find out the maximum discrepancy of a sensor reading from its nominal value (ie maximum noise). Also record the output of a couple of minutes' run of a SensorDataDisplay application in which a FaultySensorSim is substituted, as explained above. How often are the faults occurring? 

You need not submit your detailed logs but submit a brief report in each case the number of outputs, the maximum absolute discrepancy of a sensor reading from its nominal value, and the mean and standard deviation of the sensor readings.

### Main task ### 
Make a copy of SensorDataDisplay.java and change the code in the runSimulation() method so that an array of SensorSim objects are employed rather than a single one. They should all be given the same ini- tial nominal value and noise value. The reading should be an average of their readings.

Rather than hard-coding the size of this array, make it a parameter passed in through the constructor.

Make one of the simulated sensors is a FaultySensorSim and the remainder noisy but non-faulty are two other (non-faulty) SensorSim instances. Investigate the behaviour of this simulation with three (1+2) sensors: how high can the nominal value go before an emergency shut-down is triggered? Repeat with four (1+3) sensors.

Can you devise a voter function that gives better protection against `false alarm' shut-down?

One idea to reject any reading that deviates too far (more than twice the noise level) from the average of the other readings, taking this as `the' average reading.

Investigate the ability of this voting approach to protect the system from false emergency shut-down due to a single faulty sensor and give a comparison with the simple averaging approach. Submit a report of your investigation including:

1. (2 mks) a brief report of the warm-up task;
2. (2 mks) an abbrievated code listing of your modi
ed SensorDataDisplay.java employing an array of simulated sensors including a faulty one;
3. (4 mks) an abbrievated code listing of a furtherd modi
ed SensorDataDisplay.java employing a voter function;
4. (2 mks) reports of results of each of these simulations over a few minutes' run with 3 (= 1+2) and with 4 (=1+3) sensors;
5. (2 mks) your conclusions.

You may abbreviate your code listings - I just need to know where your modifications into the orginal SensorDataDisplay.java. Your report of the investigation should clearly describe the procudure you followed and the settings you used and the results you obtained.